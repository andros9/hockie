# README for hockIE 

## My information

* Andrew Rosenbaum
* andros@brandeis.edu


## Files

* writeup/
    * LaTeX and PDF
* scripts/
    * code is here
* data/
    * Scraped twitter data, timelines stored as pickle files 
    * Each file, when loaded, is a list of tweets in reverse chronological order.
* raw_seasons/
    * Raw html data scraped from http://www.flyershistory.com/cgi-bin/hspgames.cgi
* parsed_seasons/
    * Parsed data from raw_seasons and put into json format.
* anno_games/
    * Matched up games and tweets
    * I've annotated these
* todo_anno_games/
    * Matched up games and tweets
    * Not yet annotated. (All labels are "O" by default)
* guidelines.md
    * Very rough notes on my annotation. Probably useless until I clean them up.

## Running Classification Script

This takes about 10-15 minutes.

    python scripts/classify_tweets.py anno_games/NHLBruins/NHLBruins*.json


## Dependencies

Note that the the twitter scraping scripts will not work without my authentication keys!

* Python 2.x (must have argparse)
* NLTK (Natural Language Toolkit)
* NumPy
* BeautifulSoup4
    * http://www.crummy.com/software/BeautifulSoup/bs4/doc/
* python-twitter 1.3.1
    * https://github.com/bear/python-twitter

## Notes

Note: there is an error in this file:
http://www.flyershistory.com/cgi-bin/poboxscore.cgi?H20130971