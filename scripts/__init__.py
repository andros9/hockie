#!/usr/bin/python
# Usage details: python __init__.py.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: 4/26/14

from __future__ import print_function
import argparse


def main():
    parser = argparse.ArgumentParser(
        description="")
    args = parser.parse_args()


if __name__ == '__main__':
    main()
