#!/usr/bin/python
# Usage details: python run_match_season_with_tweets.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: Spring 2014

from __future__ import print_function
import argparse
import os
from subprocess import check_call
from get_tweets import seprint


def main():
    assert os.getcwd().lower().endswith('hockie')
    parser = argparse.ArgumentParser(
        description="Run the match_seasons_with_tweets script, "
                    "matching up files in parsed_seasons with files in data, "
                    "Ignoring those that have already been matched up.")
    args = parser.parse_args()

    seasons_in = [os.path.join('parsed_seasons', fn) for fn in os.listdir('parsed_seasons') if fn.endswith('.json')]
    timelines_in = [os.path.join('data', fn) for fn in os.listdir('data') if fn.endswith('.pkl')]

    seasons_in.sort()
    timelines_in.sort()

    # season_fn, timeline_fn, handle
    param_triples = []
    for season_fn, timeline_fn in zip(seasons_in, timelines_in):
        s_handle = os.path.basename(season_fn).split('-')[0]
        tl_handle = os.path.basename(timeline_fn).split('-')[0]
        assert s_handle == tl_handle
        param_triples.append((season_fn, timeline_fn, s_handle))


    for (s_fn, tl_fn, team_handle) in param_triples:
        will_be_outdir = os.path.join('./todo_anno_games', team_handle)
        if os.path.exists(will_be_outdir):
            seprint("Directory {:s} already exists. Skipping.".format(will_be_outdir))
        else:
            check_call(['python', 'scripts/match_season_with_tweets.py', s_fn, tl_fn, team_handle])


if __name__ == '__main__':
    main()
