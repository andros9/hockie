#!/usr/bin/python
# Usage details: python match_season_with_tweets.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: Spring 2014

from __future__ import print_function
import argparse
from collections import OrderedDict
import json
import os
import pickle
import datetime
from get_tweets import seprint


def main():
    assert os.path.basename(os.getcwd()).lower() == "hockie", "Must run script from root ('hockIE') directory."
    parser = argparse.ArgumentParser(
        description="For each game, outputs a json string combining the games with tweets from the same day. "
                    "Outputs to outdir/team_handle/team_handle-date.json")
    parser.add_argument("season_summary", help="json file")
    parser.add_argument("pickle_timeline", help="Twitter Timeliene for the same team")
    parser.add_argument("team_handle", help="e.g., NHLBruins")
    parser.add_argument("--outdir", "-o", default="./todo_anno_games")
    args = parser.parse_args()

    with open(args.season_summary) as sin:
        _season = json.load(sin, object_pairs_hook=OrderedDict)

    # Going to find only games that have tweets about them.
    tweeted_season = _season

    date2idx = {game["date"]: i for (i, game) in enumerate(_season)}
    max_idx = 0

    # Which games were actually tweeted about
    tweeted_indices = set()
    dates = set()

    # Load Timeline
    with open(args.pickle_timeline, 'rb') as pin:
        timeline = pickle.load(pin)

    # Match up games with tweets.
    for tweet in timeline:
        tweet_date_string = extract_date_string(tweet)
        #print(tweet_date)

        # Match
        if tweet_date_string in date2idx:
            idx = date2idx[tweet_date_string]

            # Keep track of which games tweeted about
            tweeted_indices.add(idx)
            dates.add(tweet_date_string)

            # Append to game
            summary = get_tweet_summary_dict(tweet)
            try:
                tweeted_season[idx]["tweets"].append(summary)
            except KeyError:
                tweeted_season[idx]["tweets"] = [summary]

    # Select only games that were tweeted about.
    real_tweeted_season = [game for (i, game) in enumerate(tweeted_season) if i in tweeted_indices]

    # Debug messages
    seprint("{:d} of {:d} games (max idx {:d}) had tweets".format(len(tweeted_indices), len(_season), max_idx))
    seprint("Indices: {:s}".format(str(sorted(tweeted_indices))))
    seprint("Dates: {:s}".format(str(sorted(dates))))

    # FINAL RESULT
    real_outdir = os.path.join(args.outdir, args.team_handle)
    try:
        os.mkdir(real_outdir)
    except OSError as e:
        seprint(e.message)

    for game in real_tweeted_season:
        ofp = os.path.join(real_outdir, "{:s}-{:s}.json".format(args.team_handle, game['date']))

        # DO NOT OVERWRITE
        if os.path.exists(ofp):
            seprint("File {:s} exists! Will not overwrite. Skipping...".format(ofp))
            continue

        # finally, write json string to file
        else:
            with open(ofp, 'w') as ofile:
                json.dump(game, ofile, indent=1)
            seprint("Output: {:s}".format(ofp))



def extract_date_string(tweet):
    """Returns a string of YYYY-MM-DD"""
    return datetime.date.fromtimestamp(tweet.created_at_in_seconds).strftime("%Y-%m-%d")


def get_tweet_summary_dict(tweet):
    d = OrderedDict()
    d["text"] = tweet.text
    d["label"] = "O"
    d["id"] = tweet.id
    d["hashtags"] = [ht.text for ht in tweet.hashtags]
    d["user_mentions"] = [u.screen_name for u in tweet.user_mentions]
    d["created_at"] = tweet.created_at
    d["retweet_count"] = tweet.retweet_count
    d["favorite_count"] = tweet.favorite_count
    return d



if __name__ == '__main__':
    main()
