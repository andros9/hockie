#!/usr/bin/python
# Usage details: python util.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: Spring 2014

from __future__ import print_function
import json
import twitter

auth_path = '/Users/andyrosenbaum/tools/hockIE_keys.json'
auth_keys = json.load(open(auth_path))


def get_twitter_api():
    api = twitter.Api(consumer_key=auth_keys[0],
                      consumer_secret=auth_keys[1],
                      access_token_key=auth_keys[2],
                      access_token_secret=auth_keys[3])
    return api