#!/usr/bin/python
# Usage details: python check_anno.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: Spring 2014

from __future__ import print_function
import argparse
import json
from get_tweets import seprint

#ALL_LABEL_PATTERN = r'O|START|END|PER-1|PER-2|PER-3|PER-OT|PER-SO|GOAL-[0-9]+'

ALL_VALID_LABELS = {'O', 'START', 'END', 'PER-1', 'PER-2', 'PER-3', 'PER-OT', 'PER-SO',
              'GOAL-1', 'GOAL-2', 'GOAL-3', 'GOAL-4', 'GOAL-5', 'GOAL-6', 'GOAL-7',
              'GOAL-8', 'GOAL-9', 'GOAL-10', 'GOAL-11', 'GOAL-12', 'GOAL-13', 'GOAL-14'}


def main():
    parser = argparse.ArgumentParser(
        description="Verify that the annotation is valid")
    parser.add_argument("anno_file", nargs="+", help="json file, fixed up with the annotations for 'label'")
    args = parser.parse_args()

    for af in args.anno_file:
        with open(af) as gin:
            game = json.load(gin)
        try:
            validate(game)
        except AssertionError as e:
            seprint(e.message)
            seprint("File: {:s}".format(af))
            raise e

    seprint("{:d} anno files passed.".format(len(args.anno_file)))


def validate(game):
    non_o_labeled = []
    for tweet in game['tweets']:
        label = tweet['label']
        check_label(label)

        if label != "O":
            non_o_labeled.append(label)

    # Gather freq dist
    label2count = {}
    for label in non_o_labeled:
        try:
            label2count[label] += 1
        except KeyError:
            label2count[label] = 1

    # Check appropriate number of start/end/period_breaks
    assert label2count['START'] == 1, 'START missing or too many'
    assert label2count['PER-2'] == 1, 'PER-2 missing or too many'
    assert label2count['PER-3'] == 1, 'PER-3 missing or too many'
    assert label2count['END'] == 1, 'END missing or too many'

    # Check overtime and shootout
    assert not had_overtime(game) or 'PER-OT' in label2count, 'Missing PER-OT label for OT game'
    assert not had_shootout(game) or 'PER-SO' in label2count, 'Missing PER-SO label for SO game'


def had_overtime(game):
    goal_periods = {g['period'] for g in game['goals']}

    # Goal scored in OT
    if 'OT' in goal_periods:
        return True

        # Shootout goals not counted towards total
    if int(game['num_road_team_goals']) + int(game['num_home_team_goals']) != len(game['goals']):
        return True

    return False


def had_shootout(game):

    # Shootout goals not counted towards total
    if int(game['num_road_team_goals']) + int(game['num_home_team_goals']) != len(game['goals']):
        return True


def check_label(label):
    assert label in ALL_VALID_LABELS, 'Invalid label: {:s}'.format(label)


if __name__ == '__main__':
    main()
