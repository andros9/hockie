#!/usr/bin/python
# Usage details: python parse_season_summary.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: Spring 2014

from __future__ import print_function
import argparse
import json
import re
import urllib2
from bs4 import BeautifulSoup
import datetime
from get_tweets import seprint


def main():
    parser = argparse.ArgumentParser(
        description="")
    parser.add_argument("season_page",
                        help="html document saved from http://www.flyershistory.com/cgi-bin/hspgames.cgi")
    args = parser.parse_args()

    season_page = args.season_page

    with open(season_page) as fin:
        season_soup = BeautifulSoup(fin)

    header = season_soup.find('h1').text
    header = header.strip()
    season_number, team_name = header.split(' - ')

    games_table = season_soup.findAll('table')[1]

    lines = games_table.findAll('tr')

    games = []

    for i, line in enumerate(lines[1:]):  # Skip header
        # Pass in all the separated fields
        seprint("Parsing game {:d} of {:d}".format(i+1, len(lines)-1))
        seprint(line)
        try:
            game = Game(*line.findAll('td'))
            games.append(game)
        except (IndexError, AssertionError, ValueError) as e:
            seprint(">>>> ERROR <<<<")
            seprint(e.message)

    print(json.dumps(games, indent=1))


        #print('\t'.join((str(field.text) for field in line.findAll('td'))))

    #print(games_table)


#def game_line2dict(line):
#    """Converts a game line to a dictionary"""
#    fields = line.findAll('td')


def isoformat_game_date(game_date_text):
    return datetime.datetime.strptime(game_date_text, '%A, %B %d, %Y').strftime("%Y-%m-%d")


class Game(dict):
    def __init__(self, date, road_team, road_team_goals, _at_, home_team, home_team_goals, result, summary_url):
        """
        Pass in html elements from parsing the table.
        """
        super(Game, self).__init__()
        self['date'] = isoformat_game_date(date.text)
        self['road_team'] = road_team.text
        self['num_road_team_goals'] = road_team_goals.text
        self['home_team'] = home_team.text
        self['num_home_team_goals'] = home_team_goals.text
        self['result'] = result.text
        self['summary_url'] = summary_url.a['href']
        #self['summary'] = None
        self['goals'] = self.GetGoals()

    def GetGoals(self):
        return parse_goals(self["summary_url"])

    def __str__(self):
        return "\t".join((self['date'], self['road_team'], self['road_team_goals'],
                          '@', self['home_team'], self['home_team_goals'], self['result'], self['summary_url']))

    def __hash__(self):
        """date, home_team, and road_team must match"""
        return hash((self['date'], self['home_team'], self['road_team']))

    def __eq__(self, other):
        """date, home_team, and road_team must match"""
        return (self['date'] == other['date'] and
                self['home_team'] == other['home_team'] and
                self['road_team'] == other['road_team'])


class Goal(dict):
    def __init__(self, game_index, team, player, season_index, asssists, strength, time, period):
        """
        Each parameter should be a string.

        :param game_index: Goal count this game
        :param team: team that scored (three letter abbreviation)
        :param player: player that scored
        :param season_index: goal count for player this season
        :param asssists: players who assisted. Currently: tuple as string TODO: parse this
        :param strength: (PP) (EV) or (SH)
        :param time: e.g., "12:37"
        :param period: e.g., "1", "2", "3", or "OT"
        """
        super(Goal, self).__init__()
        self['game_index'] = game_index
        self['team'] = team
        self['player'] = player
        self['season_index'] = season_index
        self['assists'] = asssists
        self['strength'] = strength
        self['time'] = time
        self['period'] = period


def parse_goals(url):
    """
    Parse summary url: extract goals
    :param url: summary link from table
    :type url: str
    :return: list of Goals
    :rtype: list(Goal)
    """
    soup = get_soup(url)
    table_comps = soup.findAll('table')[1].findAll('tr')

    # Period 1
    assert table_comps[2].h3.text == "First Period"
    goals_1 = _extract_goals(table_comps[3].text, "1")
    seprint("period 1")
    seprint(goals_1)

    # Period 2
    assert table_comps[4].h3.text == "Second Period"
    goals_2 = _extract_goals(table_comps[5].text, "2")
    seprint("period 2")
    seprint(goals_2)

    # Period 3
    assert table_comps[6].h3.text == "Third Period"
    goals_3 = _extract_goals(table_comps[7].text, "3")
    seprint("period 3")
    seprint(goals_3)

    try:
        maybe_ot = table_comps[8]
        if maybe_ot.h3.text == "Overtime":
            goals_ot = _extract_goals(table_comps[9].text, "OT")
        else:
            goals_ot = []

    except AttributeError:
        goals_ot = []

    seprint("ot")
    seprint(goals_ot)

    all_goals = goals_1 + goals_2 + goals_3 + goals_ot

    # Sanity check, make sure we got them all
    assert [int(g['game_index']) for g in all_goals] == range(1, len(all_goals) + 1)
    return all_goals


def _extract_goals(period_summary_text, period_number):
    """
    Use regex to extrac and parse goal object
    :param period_summary_text:
    :param period_number: (str)
    :return: list of Goals
    """
    seprint(period_summary_text)
    p = re.compile(
        r'([0-9]+)\s+-\s+(\S{3}) : (.*?)\s+'  # game_index, team, player
        '([0-9]+)?\s*(\(.*?\))?\s*(\([-A-Z]{2,}\))\s+'  # season_index?, assists?, strength
        '([0-9]{1,2}:[0-9]{2})'  # time
    )
    goal_tuples = [m.groups() + (str(period_number),) for m in p.finditer(period_summary_text)]
    goals = [Goal(*t) for t in goal_tuples]
    return goals


def get_soup(url):
    uo = urllib2.urlopen(url)
    soup = BeautifulSoup(uo)
    uo.close()
    return soup




if __name__ == '__main__':
    main()
