#!/usr/bin/python
# Usage details: python run_parse_seasons.py.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: Spring 2014

from __future__ import print_function
import argparse
import os
from subprocess import check_call
from get_tweets import seprint


def main():
    assert os.getcwd().lower().endswith('hockie')
    parser = argparse.ArgumentParser(
        description="Run the parse_seasons script on all files from raw_seasons that have not yet been parsed.")
    args = parser.parse_args()

    filenames_in = [os.path.join('raw_seasons', fn) for fn in os.listdir('raw_seasons')]
    filenames_out = [os.path.join("parsed_seasons", fn.replace('.html', '.json')) for fn in os.listdir("raw_seasons")]


    for fn_in, fn_out in zip(filenames_in, filenames_out):
        if os.path.exists(fn_out):
            seprint("File {:s} exists. Will not overwrite. Skipping".format(fn_out))
        else:
            seprint("Parsing and writing to {:s}".format(fn_out))
            with open(fn_out, 'w') as fout:
                check_call(['python', 'scripts/parse_season_summary.py', fn_in], stdout=fout)


    #print(filenames_in)
    #
    #print(filenames_out)


    #print(basenames_out)







if __name__ == '__main__':
    main()
