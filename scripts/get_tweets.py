#!/usr/bin/python2.7
# Usage details: python get_tweets.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: Spring 2014

from __future__ import print_function
import argparse
from datetime import datetime
import json
import os
import pickle
import subprocess
import sys
import twitter


auth_path = '/Users/andyrosenbaum/tools/hockIE_keys.json'
auth_keys = json.load(open(auth_path))


def main():
    raise RuntimeError("I already scraped everything I need for the project. Probably best to leave it alone for now...")
    parser = argparse.ArgumentParser(
        description="Get some tweets from the provided twitter handle. "
                    "Append them to what's in provided pkl file (or create it if --first), and dump "
                    "the combined results back to that file. ")
    parser.add_argument("timeline_pickle_path",
                        help="Where to save and load the pickle file. "
                             "If using first option, the file must not yet exist.")
    parser.add_argument("handle",
                        help="Twitter user's handle.")
    parser.add_argument("--first", action="store_true", default=False,
                        help="Start a new pickle file and all.")
    args = parser.parse_args()

    api = twitter.Api(consumer_key=auth_keys[0],
                      consumer_secret=auth_keys[1],
                      access_token_key=auth_keys[2],
                      access_token_secret=auth_keys[3])

    timeline_pickle_path = args.timeline_pickle_path
    handle = args.handle



    if not args.first:

        # Load existing
        with open(timeline_pickle_path, 'rb') as pin:
            old_tl = pickle.load(pin)
        seprint("Number of tweets collected so far: {:d}".format(len(old_tl)))

        # Date of oldest so far
        tail_tweet = old_tl[-1]
        tail_tweet_creation_dt = get_creation_dt(tail_tweet)
        seprint("Oldest date of tweets so far (UTC): {:s}".format(datetime.isoformat(tail_tweet_creation_dt)))

        # Next "page"
        max_id = old_tl[-1].id
        max_id -= 1  # API Hack so the tweet does not get repeated.
        tl = api.GetUserTimeline(screen_name=handle, count=200, trim_user=True, max_id=max_id)

        # Newest date of next "page"
        head_tweet = tl[0]
        head_tweet_creation_dt = get_creation_dt(head_tweet)
        seprint("Newest date of current tweets page: {:s}".format(datetime.isoformat(head_tweet_creation_dt)))
        seprint("Oldest date of current tweets page: {:s}".format(datetime.isoformat(get_creation_dt(tl[-1]))))

        # Sanity check for datetime
        assert head_tweet_creation_dt < tail_tweet_creation_dt

    else:

        if os.path.exists(timeline_pickle_path):
            raise IOError("File already {:s} exists! Will not overwrite.".format(timeline_pickle_path))

        subprocess.check_call(["touch", timeline_pickle_path])

        old_tl = []
        tl = api.GetUserTimeline(screen_name=handle, count=200, trim_user=True)


    # Combine and pickle dump
    combo_tl = old_tl + tl
    seprint("Updated number of tweets: {:d}".format(len(combo_tl)))
    with open(timeline_pickle_path, 'wb') as pout:
        pickle.dump(combo_tl, pout)
    seprint("Dumped pickled tweets to {:s}".format(timeline_pickle_path))


def seprint(message, **kwargs):
    print("# {:s}".format(message), file=sys.stderr, **kwargs)


def get_creation_dt(tweet):
    return datetime.strptime(tweet.created_at, '%a %b %d %H:%M:%S +0000 %Y')


if __name__ == '__main__':
    main()
