#!/usr/bin/python
# Usage details: python classify_tweets.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: Spring 2014
# Adapted from http://streamhacker.com/2010/05/24/text-classification-sentiment-analysis-stopwords-collocations/

from __future__ import print_function
import argparse
import collections
import json
import re
import nltk.classify.util, nltk.metrics
from nltk.classify import NaiveBayesClassifier, MaxentClassifier
from nltk import PorterStemmer
from get_tweets import seprint

stemmer = PorterStemmer()


HARD_CODED_TWEETING_TEAM = 'BOS'

def main():
    parser = argparse.ArgumentParser(
        description="Read in all annotated games, split data into training and testing, "
                    "Train and evaluate a MaxEnt classifier.")
    parser.add_argument("anno_game_fn", nargs="+", help="json format")
    args = parser.parse_args()

    anno_games = []
    for fn in args.anno_game_fn:
        if 'NHLBruins' not in fn:
            raise UserWarning("There's some hard-coded stuff right now in "
                              "get_goal_my_opp_other that only works with Bruins tweets!")
        anno_games.append(json.load(open(fn)))

    # Simple yes/no (yes meaning label is GOAL-??)
    yes_tweets, no_tweets = get_yes_no_tweets(anno_games)
    evaluate_classifier_multi({'yes': yes_tweets, 'no': no_tweets}, featx_list=[word_feats], train_portion=0.75)

    # goal_new, goal_old, other
    print("\n# ========= #\n")
    goal_new, goal_old, other = get_goal_new_old_other(anno_games)
    evaluate_classifier_multi({'goal-new': goal_new, 'goal-old': goal_old, 'other':other},
                              featx_list=[word_feats], train_portion=0.75)

    # my_goal, opp_goal, other
    print("\n# ========= #\n")
    my_goal, opp_goal, other = get_goal_my_opp_other(anno_games)
    evaluate_classifier_multi({'my-goal':my_goal, 'opp-goal': opp_goal, 'other': other},
                              featx_list=[word_feats], train_portion=0.75)


def get_yes_no_tweets(anno_games):
    """
    two lists:
      tweets with label GOAL-??
      tweets with label O
      """
    no_tweets = []
    yes_tweets = []
    for game in anno_games:
        for tweet in game['tweets']:
            if tweet['label'].startswith('GOAL'):
                yes_tweets.append(tweet)
            else:
                no_tweets.append(tweet)


    seprint("Distribution of tweets: {:d} yes, {:d} no".format(
        len(yes_tweets), len(no_tweets)))
    return yes_tweets, no_tweets


def get_goal_new_old_other(anno_games):
    """
    Three lists:
      1) tweets with label GOAL-??, where this is the first mention of the goal
      2) tweets with label GOAL-?? where this is not the first mention of the goal
      3) tweets with label O
    """
    goal_new_tweets = []
    goal_old_tweets = []
    other_tweets = []

    for game in anno_games:
        seen_goal_labels = set()
        for tweet in game['tweets']:
            label = tweet['label']
            if label.startswith('GOAL'):

                # GOAL-OLD
                if label in seen_goal_labels:
                    goal_old_tweets.append(tweet)

                # GOAL-NEW
                else:
                    goal_new_tweets.append(tweet)
                    seen_goal_labels.add(label)

            # other
            else:
                other_tweets.append(tweet)

    seprint("Distribution of tweets: {:d} goal_new, {:d} goal_old, {:d} other".format(
        len(goal_new_tweets), len(goal_old_tweets), len(other_tweets))
    )
    return goal_new_tweets, goal_old_tweets, other_tweets


def get_goal_my_opp_other(anno_games):
    """
    N.B.: I've hardcoded this assuming all the games show tweets by NHLBruins!
    Fix this method if the tweets are from another team.

    Three lists:
      1) tweets with label GOAL-?? where the goal was scored by the team tweeting about it. ("my" goal)
      2) tweets with label GOAL-?? where the goal was scored by the opposing team. ("opp" goal)
      3) tweets with any other label
    """
    my_goal_tweets = []
    opp_goal_tweets = []
    other_tweets = []

    for game in anno_games:
        goals = game['goals']
        for tweet in game['tweets']:
            label = tweet['label']
            if label.startswith('GOAL'):

                # Check type of goal
                idx = int(label.split('-')[1])  # Format is, e.g. GOAL-2
                the_goal = goals[idx-1]  # goals are 1-indexed
                assert the_goal['game_index'] == str(idx)

                # my_goal -- THIS IS HARD-CODED FOR BOSTON BRUINS
                if the_goal['team'] == HARD_CODED_TWEETING_TEAM:
                    my_goal_tweets.append(tweet)

                # opp_goal
                else:
                    opp_goal_tweets.append(tweet)

            else:
                other_tweets.append(tweet)

    seprint("Distribution of tweets: {:d} my_goal, {:d} opp_goal, {:d} other".format(
            len(my_goal_tweets), len(opp_goal_tweets), len(other_tweets))
    )

    return my_goal_tweets, opp_goal_tweets, other_tweets


def evaluate_classifier_multi(label2tweets, featx_list, train_portion=0.75):
    """
    Allows for multi-class classification

    label2tweets should be a dict, e.g.
      {'GOAL-NEW': [list_of_tweets], 'GOAL-OLD': [list_of_tweets], 'PENALTY': [list_of_tweets], ...}

    featx_list is a list of feature extracting functions
      Each one should accepts a tweet and returns a dict of {feat_name: feat_value}
    """
    #label2feats = {}
    train_feats = []
    test_feats = []
    for label, tweets in label2tweets.iteritems():
        #label2feats[label] = \
        for featx in featx_list:
            feats = [(featx(t), label) for t in tweets]
            cutoff = int(len(feats) * train_portion)
            train_feats.extend(feats[:cutoff])
            test_feats.extend(feats[cutoff:])

    #classifier = NaiveBayesClassifier.train(train_feats)
    classifier = MaxentClassifier.train(train_feats, max_iter=100)

    ref_sets = collections.defaultdict(set)
    hyp_sets = collections.defaultdict(set)

    for i, (feats, label) in enumerate(test_feats):
        ref_sets[label].add(i)
        observed = classifier.classify(feats)
        hyp_sets[observed].add(i)

    for k in ref_sets.keys():
        print()
        print('{:s} precision:'.format(k), nltk.metrics.precision(ref_sets[k], hyp_sets[k]))
        print('{:s} recall:'.format(k), nltk.metrics.recall(ref_sets[k], hyp_sets[k]))
        print('{:s} f:'.format(k), nltk.metrics.f_measure(ref_sets[k], hyp_sets[k]))


def word_feats(tweet):
    """bag of (normalized) words"""
    feats = {}

    # Some custom normalizations prior to tokenizing
    norm_words = []
    for word in tweet['text'].split():
        norm_word = normalize_word(word)
        if norm_word is not None:
            norm_words.append(norm_word)

    norm_text = ' '.join(norm_words)

    # Now, tokenize
    for norm_word in nltk.word_tokenize(norm_text):
        feats[norm_word] = True

    return feats


def normalize_word(word):
    word = word.lower()
    if re.match(r'[0-9]+(st|nd|rd|th)', word):  # Ordinal number
        word = '7th'
    elif re.match(r'[0-9]-[0-9]', word):  # Score update
        word = '2-3'
    #elif 'http://' in word:  # Link
    #    word = "_link"
    elif re.match(r'\^[a-z]+', word):  # Person who entered tweet
        word = None
    # elif re.match('[0-9]+:[0-9]+', word):
        #word = "11:11"
    return word


def bigram_feats(tweet):
    """bag of (normalized) bigrams"""
    tok_tweet = [normalize_word(word) for word in nltk.word_tokenize(tweet['text'])]
    return {bg: True for bg in nltk.bigrams(tok_tweet)}

def pos_feats(tweet):
    """Bag of POS"""
    tok_tweet = [normalize_word(word) for word in nltk.word_tokenize(tweet['text'])]
    pos_tweet = nltk.pos_tag(tok_tweet)
    return {tag: True for (word, tag) in pos_tweet}


def has_link(tweet):
    return {'_has_link': "http://" in tweet['text']}


def has_hashtag(tweet):
    return {'_has_hashtag': len(tweet['hashtags']) > 0}

if __name__ == '__main__':
    main()
