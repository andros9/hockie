#!/usr/bin/python
# Usage details: python select_tweets_by_date.py -h
# Andrew Rosenbaum
# andros@brandeis.edu
# Date: Spring 2014

from __future__ import print_function
import argparse
import pickle
import datetime


def main():
    parser = argparse.ArgumentParser(
        description="dipslay only those tweets from a particular date")
    parser.add_argument("timeline_pkl")
    parser.add_argument("date_string", help="Format is \"YYYY-MM-DD\"")
    args = parser.parse_args()

    date_to_find = datetime.datetime.strptime(args.date_string, "%Y-%m-%d").date()

    with open(args.timeline_pkl, 'rb') as pin:
        timeline = pickle.load(pin)

    matched_tweets = []

    for tweet in timeline:
        tweet_date = extract_date(tweet)
        #print(tweet_date)

        # Match
        if tweet_date == date_to_find:
            matched_tweets.append(tweet)
            continue

        # Passed date
        if tweet_date < date_to_find:
            break

    for tweet in matched_tweets:
        print("{:d}\t{:s}\n".format(tweet.id, tweet.text.encode('utf8')))


def extract_date(tweet):
    """Returns a datetime.date object"""
    return datetime.date.fromtimestamp(tweet.created_at_in_seconds)




if __name__ == '__main__':
    main()
