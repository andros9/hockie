\documentclass[12pt,twoside]{article}
\usepackage[hmargin=2.4cm, vmargin=3.0cm]{geometry}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{amsthm}
\usepackage{fancyhdr}
\usepackage{color}
\usepackage{pdfpages}
\usepackage{hyperref}

% Compact itemize
\usepackage{paralist}
\renewcommand{\itemize}{\compactitem}

% \newtheorem{prop}{Proposition}
% \theoremstyle{definition}
% \newtheorem*{definition}{Definition}
% \newtheorem*{prob}{Problem}
% \theoremstyle{remark}
% \newtheorem*{case}{Case}
% \newtheorem*{remark}{Remark}
% \newtheorem{subcase}{Subcase}
% \newtheorem{example}{Example}

% \newcommand{\Z}{{\mathbb Z}}
% \newcommand{\R}{{\mathbb R}}
% \newcommand{\F}{{\mathbb F}}
% \newcommand{\C}{{\mathbb C}}
% \newcommand{\Q}{{\mathbb Q}}
% \newcommand{\N}{{\mathbb N}}
% \newcommand{\E}{{\mathbb E}}
% \newcommand{\dd}{{\mathrm d}}
% \newcommand{\answer}[1]{{\color{blue} #1}}
% \newcommand{\defeq}{\vcentcolon=}


\title{HockIE: A System to Extract Information from Twitter Posts by NHL Hockey Teams.}
\date{Spring 2014}
\author{Andrew Rosenbaum (andros@brandeis.edu)}

\pagestyle{fancy}
\fancyhead[R]{CS137 Final Project}
\fancyhead[L]{Andrew Rosenbaum}

\begin{document}

\maketitle

\section{Background} % (fold)
\label{sec:background}

Twitter\footnote{http://www.twitter.com} is a social networking site where users can post short messages (``tweets'') of up to 140 characters. Of the 30 teams in the National Hockey League, 29 have official Twitter accounts. These twitter feeds are very popular. For example, the Boston Bruins\footnote{http://www.twitter.com/nhlbruins} have over 500,000 followers as of the time this document is written (May 2014). 

The Bruins and most other teams post play-by-play Twitter updates (``tweets'') during each televised hockey game. Later on, the most important facts about each game (e.g., goals, penalties, ``hits'') are posted online in a database-style format.

The goal\footnote{Pun intended.} of this project is to automatically detect which tweets correspond to which events in the game. In particular, I have focused on detecting tweets that either announce a goal is scored, or provide additional information on a recently announced goal. Here is an example, from a post by \texttt{@NHLBruins} on April 5, 2014:

\begin{figure}[h]
\fbox{\parbox{0.9\textwidth}{

\texttt{(2:40PM) ``GOAL! Krejci puts \#NHLBruins up 1-0 with 4:04 left in the first''}
\\

\texttt{(2:41PM) ``David Krejci pulls the puck out of the scramble \& goes upstairs on Emery, after Eriksson had his two bids stopped''}
\\

\texttt{(2:42PM) ``Mark that as David Krejci's 17th goal of the season, assisted by Loui Eriksson (23rd assist) \& Milan Lucic (34th). 1-0 \#NHLBruins''}
}}
\caption{Tweets by \texttt{NHLBruins} referring to the same event: a goal by David Krejci.}
\end{figure}

% section background (end)

\section{Data} % (fold)
\label{sec:data}

\subsection{Twitter feeds} % (fold)
\label{sub:twitter_feeds}

From each of the 29 NHL teams, I collected approximately 3,000 tweets\footnote{The twitter API places this limitation on how far back you can look.} for a total of $\sim$80,000 tweets. These comprise an estimated 900 games.\footnote{Some accounts post more in between games than others, and that affects how far back in time the 3,000 tweets go.} With the NHL average of about 5 goals per game\footnote{http://www.quanthockey.com/TS/TS\_GoalsPerGame.php}, the corpus consists of reports on an estimated 4500 goals in all.

\begin{table}[h]
	\begin{center}
	\begin{tabular}{|c | c || c | c | c|} \hline
		games & goals & total tweets & tweets on game days & tweets about goals \\ \hline
		900    & 4500   &  80,000  & 50,000  & $\geq$ 4500 \\
		\hline
	\end{tabular}
	\end{center}
	\caption{Entire Corpus Distribution (Estimated)}
	\label{label}
\end{table}


% subsection twitter_feeds (end)

\subsection{Game Summaries} % (fold)
\label{sub:game_summaries}

I extracted the game summaries from \texttt{http://www.flyershistory.com/cgi-bin/hspgames.cgi} for the 2013-2014 NHL regular season. I have summaries from 2,372 games, comprising 12,641 goals. 

Here is an example of a goal entry (corresponding to the tweets shown above.)

\begin{figure}[h]
\fbox{\parbox{0.9\textwidth}{
\texttt{1 - BOS : D.Krejci 17 (L.Eriksson, M.Lucic) (EV) 15:56}
}}
\caption{Database-style entry to David Krejci's goal.}
\end{figure}

% subsection game_summaries (end)

\subsection{Annotated Corpus} % (fold)
\label{sub:annotated_corpus}

Due to time constraints, I was only able to annotate a small subset of the corpus. I considered only tweets from the Boston Bruins \texttt{@NHLBruins} on days when they played a game. In total, this comprises 25 games, 2189 tweets, 131 goals, and 236 tweets about a goal.

\begin{table}[h]
	\begin{center}
	\begin{tabular}{|c | c || c | c|} \hline
		games & goals & total tweets (only game days) &  tweets about goals \\ \hline
		25    & 131   &  2189        & 236 \\
		\hline
	\end{tabular}
	\end{center}
	\caption{Annotated Corpus Distribution}
	\label{label}
\end{table}

% subsection annotated_corpus (end)

% section data (end)

\section{Experiments} % (fold)
\label{sec:experiments}

\subsection{Binary Classification} % (fold)
\label{sub:binary_classification}

% subsection binary_classification (end)

As a baseline, I attempted to simply detect whether or not a tweet mentions some goal. I split the data into a 75\% train and 25\% test, resulting in the following distribution:

\begin{table}[h]
	\begin{center}
	\begin{tabular}{|c||c|c|} \hline
		& train & \textbf{test} \\ \hline
	yes & 177  &  \textbf{59} \\ \hline
	no &  1464 &  \textbf{489} \\ \hline
	\end{tabular}
	\end{center}
	\caption{Distribution of instances for binary classification task.}
\end{table}

\subsubsection{NaiveBayes} % (fold)
\label{ssub:naivebayes}

% subsubsection naivebayes (end)

I used NLTK's NaiveBayes classifier with word unigrams (lowercased). I also collapsed all ordinal numbers (e.g., `21st' or `7th'), into the token `nth', all `score-like' strings into a canonical form, (e.g., `2-0', maps to `n-n'), and a few other normalizations. (I also tried stemming the words, but that degraded performance.)

Here are the best results so far using normalized unigrams:

\begin{table}[h]
	\begin{center}
	\begin{tabular}{|c||c|c|c|} \hline
		 & p & r & f \\ \hline 
	   yes & 0.456 & 0.967 & 0.620 \\ \hline
	   no & 0.995 & 0.861 & 0.923 \\ \hline
	\end{tabular}
	\end{center}
	\caption{Baseline detection of GOAL label with NaiveBayes.}
	\label{table:bin_nb}
\end{table}

\subsubsection{MaxEnt} % (fold)
\label{ssub:bin_MaxEnt}

% subsubsection subsubsection_name (end)

NLTK's Maximum Entropy classifier with 100 max iterations performed significantly better, but with precision and recall skewed very differently (I'm not sure why \ldots):

\begin{table}[h]
	\begin{center}
	\begin{tabular}{|c||c|c|c|} \hline
		 & p & r & f \\ \hline
	   yes & \textbf{0.889} & \textbf{0.542} & \textbf{0.674} \\ \hline
	   no & \textbf{0.947} & \textbf{0.992} & \textbf{0.969} \\ \hline
	\end{tabular}
	\end{center}
	\caption{Baseline detection of GOAL label with MaxEnt.}
	\label{table:bin_me}
\end{table}

\subsubsection{Binary Classification: Summary} % (fold)
\label{ssub:binary_classification_summary}

% subsubsection binary_classification_summary (end)

It seems that unigram features make a strong baseline for this task. It is not surprising that performance is higher on the `no' label, as the data is heavily skewed towards tweets in the `no' class. However, we are more interested in the `yes' examples in the end, so there is some room to improve, so this motivates the task.


\subsection{Discourse-New vs. Discourse-Old} % (fold)
\label{sub:classification_discourse_new_old}


The task here is to detect whether a tweet mentions a new goal, elaborates on a goal already mentioned\footnote{Note however, that this does not identify \emph{which} goal it elaborates on}, or does not mention a goal at all. Here is the distribution of the data.

\begin{table}[h]
	\begin{center}
	\begin{tabular}{|c||c|c|} \hline
       		 & train & \textbf{test} \\ \hline
	goal-new & 98  &  \textbf{33} \\ \hline
	goal-old &  78 &  \textbf{27} \\ \hline
	other    &  1464 &  \textbf{489} \\ \hline
	\end{tabular}
	\end{center}
	\caption{Distribution of instances for discourse new/old vs. other classification task.}
\end{table}

\subsubsection{MaxEnt} % (fold)
\label{ssub:dno_maxent}

% subsubsection maxent (end)


Again, I used NLTK's MaxEnt classifier with 100 iterations, and, for now, just normalized unigrams as features.

\begin{table}[h]
	\begin{center}
	\begin{tabular}{|c||c|c|c|} \hline
		     & p     & r     & f \\ \hline
	goal-new & 0.636 & 0.212 & 0.318 \\ \hline
	goal-old & 0.429 & 0.222 & 0.293 \\ \hline
	other    & 0.926 & 0.992 & 0.958 \\ \hline
	\end{tabular}
	\end{center}
	\caption{MaxEnt Classification of discourse new/old vs. other, with unigram feats.}
\end{table}

% subsection classification_discourse_new_old (end)

\subsubsection{Discourse-New vs. Discourse-Old: Summary} % (fold)
\label{ssub:discourse_new_vs_discourse_old_summary}

The performance was much lower on this task. I suspect data sparsity might be part of the problem here. Also, during annotation, I noticed that many of the `goal-old' (elaboration) tweets look very similar to tweets mentioning other events, such as a penalty, an exciting play, or an almost-goal.

% subsubsection discourse_new_vs_discourse_old_summary (end)

\subsection{My Goal vs. Opponent Goal} % (fold)
\label{sub:my_goal_vs_opponent_goal}

The task here is to detect whether a tweet mentions a goal scored by the same team tweeting about it (``my goal''), about a goal scored by the opposing team (``opponent goal''), or about something else (``other''). Here is the data distribution:

\begin{table}[h]
	\begin{center}
	\begin{tabular}{|c||c|c|} \hline
       		 & train & \textbf{test} \\ \hline
	my-goal & 135  &  \textbf{45} \\ \hline
	opp-goal &  42 &  \textbf{14} \\ \hline
	other    &  1464 &  \textbf{489} \\ \hline
	\end{tabular}
	\end{center}
	\caption{Distribution of instances for ``my goal vs. opponent goal vs. other'' task}
\end{table}

% subsection my_goal_vs_opponent_goal (end)

\subsubsection{MaxEnt} % (fold)
\label{ssub:mo_maxent}

I again used NLTK's MaxEnt Classifier with 100 max iterations, and normalized unigrams as features.

\begin{table}[h]
	\begin{center}
	\begin{tabular}{|c||c|c|c|} \hline
		     & p     & r     & f \\ \hline
	my-goal  & 0.810 & 0.378 & 0.515 \\ \hline
	opp-goal & 0.800 & 0.286 & 0.421 \\ \hline
	other    & 0.931 & 0.994 & 0.961 \\ \hline
	\end{tabular}
	\end{center}
	\caption{MaxEnt Classification of my goal vs. opp goal vs. other, with unigram feats.}
\end{table}

\subsubsection{My Goal vs. Opponent Goal: Summary} % (fold)
\label{ssub:my_goal_vs_opponent_goal_summary}

The performance here was lower than the binary classification task, but higher than the discourse old-new task. Since I'm only using unigrams as features, I suspect that this reflects the intuition that a team tweeting about its own goals will use very different words than when it is tweeting about goals by the opposing team. 

For example, in many (but not all) of the examples in the annotated corpus, the first discourse-new tweet about a ``my goal'' starts with the word `GOAL!'.

% subsubsection my_goal_vs_opponent_goal_summary (end)


% subsubsection maxent (end)
% section experiments (end)

\newpage


\section{Future Work} % (fold)
\label{sec:future_work}

\subsection{Features} % (fold)
\label{sub:features}

% subsection features (end)

Some other features to explore:
\begin{itemize}
	\item Named Entities in tweets.
	\item Number of times the tweet was `retweeted' or `favorited'.
	\item Normalizing hashtags of opponent's team, e.g., \{\#Rangers, \#Penguins, \ldots \} $\rightarrow$ \#Opponent
	\item Using a gazetteer of player names from both teams.
\end{itemize}

\subsection{Classification Approaches} % (fold)
\label{sub:classification_approaches}

% subsection classification_approaches (end)


Some other classification tasks to explore:
\begin{itemize}
	\item Given tweets labeled generically as `GOAL', \emph{and} the game summary, match up which tweets go with which goals.
	\item Considering the day's twitter feed as a sequence, use a CRF to label the tweets in order.
	\begin{itemize}
		\item Constraints might help here, e.g. requiring (or strongly preferring) that goals are reported in order.
	\end{itemize}
	\item Extend the annotation scheme, e.g., to also include, penalties.
\end{itemize}

% section future_work (end)


\section{Acknowledgements} % (fold)
\label{sec:acknowledgements}

I would like to thank Professor Nianwen ``Bert'' Xue and Teaching Assistant Chuan Wang for a great semester. I learned a lot in this class and really enjoyed it!

% section conclusion (end)

\end{document}