# Guidelines for HockIE

## Goals

In general, annotate as "GOAL-n" any tweet that announces a goal, or provides additional detail about the goal. However, it must *explicitly* mentioning the goal it's referring to.



### Labels

* Annotate a tweet as either "O" or "GOAL-n"
    * where 'n' refers to the index of that goal in this game.
    * **N.B.: 'n' starts at 1**

### First mention

In general, first mention is most important.

* "text": "GOAL! David Krejci off the post and in. #NHLBruins up 3-0 with 47.9 left in the second ^CS"
    * "label": "GOAL-3"

### Other mentions

Annotate other mentions too.

* text: "David Krejci's second of the game, giving him 15 goals on the season &amp; Boston the three-goal lead ^CS"
    * label: "GOAL-3"

### Examples

* "GOAL! Big Zee bangs the rebound home off Krug's drive on the PP ^CS", 
    * label: "GOAL-5"
* "Goal under review ^CS", 
    * label: "GOAL-5"
* "Good Goal! Chara makes it a one-goal game 11:01 into the 2nd ^CS", 
    * label: "GOAL-5"
* "9 of Zee's 14 goals this season have now come on the power play ^CS #NHLBruins", 
    * label: "O"

### Summarizing points

Do NOT annotate

* "4 points in 2 games for Krejci (2 goals tonight, 2 assists vs NYR) ^CS", 
    * label: "O"

### Difficult cases

* "text": "Horn sounds on the first. 1-1 game. #NHLBruins goal from Jarome Iginla ^CS", 
    * label: "GOAL-2"

* "Horn sounds on the first. #NHLBruins take 2-0 lead to the 2nd. Goals from David Krejci &amp; Jarome Iginla ^CS"
    * label: "GOAL-2"
    * (Go with the most recent goal)

* "text": "Marchy takes the give-and-go from Bergeron, puts it past Brodeur to give the B's the lead again ^JI", 
    * label: "GOAL-4"
    * (Strong enough reference)

* "Turnover &gt; Krejci starts rush &gt; Lucic feeds it back to Krejci, who lets off a shot while getting tripped &gt; Lucic puts it home ^CS", 
    * Label: "GOAL-2"
    * (Elaboration, OK)
    
* "Meszaros sends a perfect pass to Reilly at the top of the left circle, gives it to Bergy who puts it in five-hole ^JI", 
    * Label: "GOAL-5"

* "text": "Andrej Meszaros with the other assist on Bergy's strike ^JI", 
    * Label: "GOAL-3"

* "text": "Iggy's sixth goal in his last 4 games ties Johnny Bucyk for 25th on the all-time goals list! He has 556 #congratsiggy ^JI", 
    * Label: "GOAL-4"


* "Patrice Bergeron tips in his 24th goal off the season, off the drive from Matt Bartkowski ^CS", 
    * label: "GOAL-1"
    * (Explicitly links this goal to his "24th goal of the season")

* "Bergeron with a goal in each of his past 5 games ^CS", 
    * label: "O"
    *(Does not *expliticly* reference the goal just scored.)


* "Jarome Iginla nets Goal No. 30 of the season and Goal No. 2 of the game. #NHLBruins http://t.co/qTD2hRybSx",
    * label: "O"
    * (Link to picture, not actually reporting, so, label is "O")

### Negative Examples -- too vague / distant 

* "Iggy fired up; goal came not long after he got in a heated \"bout\" of shoves with Marc Staal after the whistle in front of Lundqvist ^CS"
    * label: "O"
    * (Reference to goal is too vague/distant)

* "3 goals in 3 games for Iginla; extends his point streak to 5 games (5 goals, 1 assist) ^CS", 
    * label: "O"

* "That's 2 goals in three games for Kells, and Soderberg now has 7 points in his last 8 ^JI", 
    * label: "O"
    
* "That gives Bergy goals in three straight games, and major props to Meszaros for keeping that play alive by forcing the TO ^JI", 
    * label: "O"

* "2 goals in 13 seconds from Soderberg &amp; Bergeron. 3-0 game ^CS", 
    * label: "O"
    



### Must be first *real* mention

* "Appears #NHLBruins go up 5-2, but under review. Paille drove down the left side, with Campbell driving the net, hit his skate &amp; went in ^CS", 
    * "label": "O",
* "Good Goal! \"deflection not a direction - it's a good goal\" ^CS"
   * "label": "GOAL-7",


### Shootout

Do not mark any shootout goals.

## End

Mark a tweet signaling the end of a game

* "text": "Bruins fall 4-3 in OT ^CS", 
    * label: "END"

Shootouts are ignored in this annotation scheme.

Mark, e.g. "Going to shoutout" as "END"

## Start

Mark a tweet signaling the start of a game

* "text": "Puck dropped! #NHLBruins vs #Leafs underway from Air Canada Centre ^CS", 
    * "label": "START", 

## PER-2, PER-3, PER-OT

Mark a tweet signaling the start of a new period.